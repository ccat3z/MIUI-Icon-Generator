package cc.c0ldcat.miuiicongenerator.iconhandler;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import cc.c0ldcat.miuiicongenerator.R;
import cc.c0ldcat.miuiicongenerator.UserInfoLogListener;
import cc.c0ldcat.miuiicongenerator.utils.Common;
import cc.c0ldcat.miuiicongenerator.utils.RootHelper;
import cc.c0ldcat.miuiicongenerator.utils.ZipCompressor;
import cc.c0ldcat.miuiicongenerator.utils.ZipUncompress;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;

public class NovaHandler {
    private RootHelper rh;
    private String cacheDirPath;
    private Context context;
    private UserInfoLogListener userInfoLogListener;

    private static final String DATA_PATH = "/data/data";
    private static final String PACKAGE_NAME = "com.teslacoilsw.launcher";
    private static final String DATABASE_PATH = "databases/launcher.db";

    private static final String ICON_TABLE = "allapps";
    private static final String[] ICON_COLUMS = {"componentName", "icon"};

    private static final HashMap<String, String> MTZ_ASSETS = new HashMap<String, String>() {{
        put("description.xml", "mtz/description.xml");
    }};

    private final static String compressedIconFileName = "icons";

    public NovaHandler(RootHelper rh, String cacheDirPath, Context context, UserInfoLogListener userInfoLogListener) {
        this.rh = rh;
        this.cacheDirPath = cacheDirPath;
        this.context = context;
        this.userInfoLogListener = userInfoLogListener;
    }

    public void exportMTZ(String dstMTZPath) throws FileNotFoundException {
        // create mtz
        userInfoLogListener.showLog(context.getString(R.string.nova_compress_mtz));
        ZipCompressor mtz = new ZipCompressor(dstMTZPath);

        // generate icon
        userInfoLogListener.showLog(context.getString(R.string.nova_gen_icon));
        exportIcon();
        mtz.compress(Common.concatPath(cacheDirPath, compressedIconFileName));

        // generate other file
        AssetManager assetManager = context.getAssets();
        for (Map.Entry<String, String> entry : MTZ_ASSETS.entrySet()) {
            try {
                mtz.compress(assetManager.open(entry.getValue()), entry.getKey());
            } catch (IOException e) {
                // ignore
                Log.e(Common.DEBUG_FLAG, "compress " + entry.getValue() + " error: " + e.getMessage());
            }
        }

        // close mtz
        userInfoLogListener.showLog(context.getString(R.string.nova_done));
        mtz.close();
    }

    private void exportIcon() throws FileNotFoundException {
        final String srcDBPath = Common.concatPath(DATA_PATH, PACKAGE_NAME, DATABASE_PATH);
        final String dstDBPath = Common.concatPath(cacheDirPath, PACKAGE_NAME + ".db");

        final String srcBaseIconFilePath = "/data/system/theme/icons";
        final String dstBaseIconFilePath = Common.concatPath(cacheDirPath, "base_icons");

        final String iconsPackageWorkDirPath = Common.concatPath(cacheDirPath, "export_icons");
        final String exportIconsRootPath = Common.concatPath(iconsPackageWorkDirPath, "res");

        // remove cache
        File iconsPackageWorkDir = new File(iconsPackageWorkDirPath);
        if (iconsPackageWorkDir.exists()) {
            try {
                FileUtils.deleteDirectory(iconsPackageWorkDir);
            } catch (IOException e) {
                // ignore
            }
        }

        // copy database file
        try {
            userInfoLogListener.showLog(context.getString(R.string.nova_copying_database));
            rh.copyFile(srcDBPath, dstDBPath);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(context.getString(R.string.nova_no_src_database));
        }

        // copy icons file
        try {
            userInfoLogListener.showLog(context.getString(R.string.nova_copying_base_icons));
            rh.copyFile(srcBaseIconFilePath, dstBaseIconFilePath);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException(context.getString(R.string.nova_no_src_base_icons));
        }

        // uncompress icons file
        ZipUncompress.unzip(dstBaseIconFilePath, iconsPackageWorkDir);

        // icon output root dir
        List<File> cacheIconDirs = Arrays.asList(new File(exportIconsRootPath).listFiles());

        // get data in database
        SQLiteDatabase launcherDataBase = SQLiteDatabase.openDatabase(dstDBPath, null, SQLiteDatabase.OPEN_READONLY);
        Cursor iconDataCursor = launcherDataBase.query(ICON_TABLE, ICON_COLUMS, null, null, null, null, null);

        // save icon
        for (iconDataCursor.moveToFirst(); ! iconDataCursor.isAfterLast(); iconDataCursor.moveToNext()) {
            String app = iconDataCursor.getString(iconDataCursor.getColumnIndex(ICON_COLUMS[0])).split("/")[0];

            for (File cacheIconDir : cacheIconDirs) {
                try {
                    byte[] data = iconDataCursor.getBlob(iconDataCursor.getColumnIndex(ICON_COLUMS[1]));
                    File iconFile = new File(cacheIconDir, app + ".png");
                    try (FileOutputStream iconOut = new FileOutputStream(iconFile)) {
                        iconOut.write(data);
                    } catch (IOException e) {
                        // ignore
                        Log.e(Common.DEBUG_FLAG, "write icon blob of " + app + " failed");
                    }
                } catch (Exception e) {
                    // ignore this icon
                    Log.e(Common.DEBUG_FLAG, "get icon blob of " + app + " failed");
                }
            }

            userInfoLogListener.showLog(context.getString(R.string.nova_export_icon, app));
        }
        iconDataCursor.close();
        launcherDataBase.close();

        userInfoLogListener.showLog(context.getString(R.string.nova_compress_icons));
        ZipCompressor icons = new ZipCompressor(Common.concatPath(cacheDirPath, compressedIconFileName));
        icons.compress(iconsPackageWorkDir, true);
        icons.close();
    }
}
