package cc.c0ldcat.miuiicongenerator.utils;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

public class Common {
    public static String DEBUG_FLAG = "C0C_MIG";

    public static String concatPath(String... paths) {
        return concatPath(Arrays.asList(paths));
    }

    public static String concatPath(List<String> paths) {
        StringWriter sw = new StringWriter();
        for (String path : paths) {
            sw.append(path.replaceAll("/$",""));
            sw.append('/');
        }
        return sw.toString().replaceAll("/$","");
    }
}
