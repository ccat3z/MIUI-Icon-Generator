package cc.c0ldcat.miuiicongenerator.utils;

import java.io.*;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipCompressor {
    private static final int BUFFER = 8192;

    private ZipOutputStream zipOutputStream;

    public ZipCompressor(String pathName) throws FileNotFoundException {
        zipOutputStream = new ZipOutputStream(new CheckedOutputStream(new FileOutputStream(new File(pathName)), new CRC32()));
    }

    public void compress(String srcPathName) throws FileNotFoundException {
        compress(new File(srcPathName));
    }

    public void compress(File src) throws FileNotFoundException {
        compress(src, zipOutputStream, "");
    }

    public void compress(String srcDirPathName, boolean asRoot) throws FileNotFoundException {
       compress(new File(srcDirPathName), asRoot);
    }

    public void compress(File srcDir, boolean asRoot) throws FileNotFoundException {
        compressDirectory(srcDir, zipOutputStream, "", asRoot);
    }

    public void compress(InputStream in, String filepath) {
            compress(in, zipOutputStream, filepath);
    }

    public void close() {
        try {
            zipOutputStream.close();
        } catch (IOException e) {
            // ignore
        }
    }

    private void compress(File file, ZipOutputStream out, String basedir) throws FileNotFoundException {
        if (file.isDirectory()) {
            this.compressDirectory(file, out, basedir);
        } else {
            this.compressFile(file, out, basedir);
        }
    }

    private void compress(InputStream in, ZipOutputStream out, String filepath) {
        try (BufferedInputStream bis = new BufferedInputStream(in)){
            ZipEntry entry = new ZipEntry(filepath);
            out.putNextEntry(entry);
            int count;
            byte data[] = new byte[BUFFER];
            while ((count = bis.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void compressDirectory(File dir, ZipOutputStream out, String basedir) throws FileNotFoundException {
        compressDirectory(dir, out, basedir, false);
    }

    private void compressDirectory(File dir, ZipOutputStream out, String basedir, boolean asRoot) throws FileNotFoundException {
        File[] files = dir.listFiles();
        for (File file : files)
            compress(file, out, basedir + (asRoot ? "" : dir.getName() + "/" ));
    }

    private void compressFile(File file, ZipOutputStream out, String basedir) throws FileNotFoundException {
            compress(new FileInputStream(file), out, basedir + file.getName());
    }
}