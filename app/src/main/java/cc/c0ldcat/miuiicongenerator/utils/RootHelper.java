package cc.c0ldcat.miuiicongenerator.utils;

import android.util.Log;
import org.apache.commons.io.FileUtils;

import java.io.*;

public class RootHelper {
    private Process suProcess;
    private BufferedReader suIn;
    private BufferedWriter suOu;

    private static final String CMD_END_FLAG = "END_FLAG";

    public RootHelper() throws NonRootException{
        try {
            suProcess = Runtime.getRuntime().exec("su");

            suIn = new BufferedReader(new InputStreamReader(suProcess.getInputStream()));
            suOu = new BufferedWriter(new OutputStreamWriter(suProcess.getOutputStream()));

            if (! checkRoot()) throw new NonRootException();
        } catch (IOException e) {
            throw new NonRootException();
        }
    }

    public Process getSuProcess() {
        return suProcess;
    }

    public boolean checkRoot() {
        try {
            suOu.write("id\n");
            suOu.flush();
            return suIn.readLine().contains("uid=0");
        } catch (Exception e) {
            return false;
        }
    }

    public String execResult(String cmd) {
        try {
            suOu.write(cmd + "; echo " + CMD_END_FLAG + "\n");
            suOu.flush();

            String result = "";

            while (! (result += "\n" + suIn.readLine()).endsWith(CMD_END_FLAG));
            result = result.split(CMD_END_FLAG)[0].substring(1).replaceAll("\n$", "");

            return result;
        } catch (IOException e) {
            return null;
        }
    }

    public void stop() {
        try {
            suOu.write("exit\n");
            suOu.flush();

            suOu.close();
            suIn.close();
        } catch (IOException e) {

        }
    }

    public void copyFile(String srcPath, String dstPath) throws FileNotFoundException {
        // check orig database
        if (! this.execResult("[ -f " + srcPath + " ]&&echo flag").equals("flag"))
            throw new FileNotFoundException("File not found: " + srcPath);

        // touch database file due to SELinux
        this.execResult("rm " + dstPath);
        File dstDB = new File(dstPath);
        try {
            FileUtils.touch(dstDB);
        } catch (IOException e) {
            // ignore
            Log.e(Common.DEBUG_FLAG, "touch " + dstPath + " failed");
        }

        // copy database file
        this.execResult("cat " + srcPath + " > " + dstPath);
        this.execResult("chmod 666 " + dstPath);
    }

    public class NonRootException extends Exception {

    }
}
