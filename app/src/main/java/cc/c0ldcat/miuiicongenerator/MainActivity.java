package cc.c0ldcat.miuiicongenerator;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import cc.c0ldcat.miuiicongenerator.iconhandler.NovaHandler;
import cc.c0ldcat.miuiicongenerator.utils.Common;
import cc.c0ldcat.miuiicongenerator.utils.RootHelper;

import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity implements UserInfoLogListener, View.OnClickListener {
    private TextView logView;
    private Button processButton;
    private RootHelper rh;

    private static final String OUT_MTZ_PATH = Common.concatPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), "out.mtz");

    private static final int PERMISSIONS_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logView = (TextView) findViewById(R.id.logView);
        processButton = (Button) findViewById(R.id.process);

        processButton.setOnClickListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (rh != null) {
            rh.stop();
            rh = null;
        }
    }

    @Override
    public void onClick(View view) {
        try {
            if (rh == null) {
                rh = new RootHelper();
            } else if (! rh.checkRoot()) {
                rh.stop();
                rh = new RootHelper();
            }
            showLog(getString(R.string.get_root));

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST);
            } else {
                exportDatabase();
            }
        } catch (RootHelper.NonRootException e) {
            showLog(getString(R.string.no_root));
        }
    }

    public void showLog(String msg) {
        logView.setText(msg);
    }

    private void exportDatabase() {
        new ExportMTZTask(this).execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    exportDatabase();
                else
                    showLog(getString(R.string.no_permission));
                break;
        }
    }

    private class ExportMTZTask extends AsyncTask<Void, String, Void> implements UserInfoLogListener {
        private MainActivity activity;

        public ExportMTZTask(MainActivity activity) {
            this.activity = activity;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                NovaHandler ih = new NovaHandler(rh, getCacheDir().getPath(), getApplication(), this);
                ih.exportMTZ(OUT_MTZ_PATH);
            } catch (FileNotFoundException e) {
                showLog(e.getMessage());
            }
            return null;
        }

        public void showLog(String msg) {
            publishProgress(msg);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            if (activity != null)
                activity.showLog(values[0]);
        }
    }
}
