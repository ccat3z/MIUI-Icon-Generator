package cc.c0ldcat.miuiicongenerator;

public interface UserInfoLogListener {
    void showLog(String msg);
}
