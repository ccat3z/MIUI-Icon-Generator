var bigicon = File.openDialog("Select a 1024x1024 image: ", "*.png", false);

var pngDoc = open(bigicon, OpenDocumentType.PNG);
var destFolder = Folder.selectDialog( "Output dir: ");

var icons = 
[
  {"name": "hdpi", "size":72},
  {"name": "mdpi", "size":48},
  {"name": "xhdpi", "size":96},
  {"name": "xxhdpi", "size":144},
  {"name": "xxxhdpi", "size":192}
];

var option = new PNGSaveOptions();

option.PNG8 = false;
var startState = pngDoc.historyStates[0];

for (var i = 0; i < icons.length; i++) 
{
    var icon = icons[i];
    pngDoc.resizeImage(icon.size/(2041/72), icon.size/(2041/72));

    var destFilePath = "mipmap-" + icon.name + "/" + "ic_launcher.png";
    var file = new File(destFolder + "/" + destFilePath);
    pngDoc.saveAs(file, option, true, Extension.LOWERCASE);
    pngDoc.activeHistoryState = startState;
}

pngDoc.close(SaveOptions.DONOTSAVECHANGES);